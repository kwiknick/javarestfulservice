package com.youtube.rest.status;
//This is a walkthrough of a Tutorial on REST on Youtube
//The url is: https://www.youtube.com/watch?v=4DY46f-LZ0M
//I'm currently at 24:37
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/v1/status")
public class V1_Status {

	private static final String api_version = "00.01.00";
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String returnTitle() {
		return "<p>Java Web Service</p>";
	}
	@Path("/version")
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String returnVersion() {
		return "<p>Version:</p>" + api_version;
	}
}
